export class Stock {
  id: string;
  name: string;
  currentPrice: string;
  lastUpdate: string;
}
