import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Stock } from '../models/stock.model';
import { StockService } from './stock.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html'
})
export class StockComponent implements OnInit {

  stocks: Stock[];

  constructor(private router: Router, private stockService: StockService) { }

  ngOnInit() {
    this.stockService.getStocks()
      .subscribe( data => {
        this.stocks = data;
      });
  };

  editStock(stock: Stock): void {
    localStorage.removeItem("editStockId");
    localStorage.setItem("editStockId", stock.id.toString());
    this.router.navigate(['edit-stock']);
  };

}
