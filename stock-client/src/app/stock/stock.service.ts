import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Stock } from '../models/stock.model';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class StockService {

  constructor(private http:HttpClient) {}

  private stockUrl = 'http://localhost:8080/api/';

  getStockById(id: number) {
    return this.http.get<Stock>(this.stockUrl + '/stock/' + id);
  }

  public getStocks() {
    return this.http.get<Stock[]>(this.stockUrl + "/stocks");
  }

  public deleteStock(stock) {
    return this.http.delete(this.stockUrl + "/"+ stock.id);
  }

  public createStock(stock) {
    return this.http.post<Stock>(this.stockUrl + "/stocks", stock);
  }

  public updateStock(stock: Stock) {
    return this.http.put(this.stockUrl + '/stocks/' + stock.id, stock);
  }

}
