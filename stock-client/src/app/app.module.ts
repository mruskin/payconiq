import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from "@angular/forms";
import { AppComponent } from './app.component';
import { StockComponent } from './stock/stock.component';
import { AppRoutingModule } from './app.routing.module';
import {StockService} from './stock/stock.service';
import {HttpClientModule} from "@angular/common/http";
import {AddStockComponent} from './stock-add/add-stock.component';
import {EditStockComponent} from './stock-edit/edit-stock.component';

@NgModule({
  declarations: [
    AppComponent,
    StockComponent,
    AddStockComponent,
    EditStockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [StockService],
  bootstrap: [AppComponent]
})
export class AppModule { }
