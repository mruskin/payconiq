import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StockComponent } from './stock/stock.component';
import { AddStockComponent } from "./stock-add/add-stock.component";
import { EditStockComponent } from './stock-edit/edit-stock.component';

const routes: Routes = [
  { path: 'stocks', component: StockComponent },
  { path: 'add', component: AddStockComponent },
  { path: 'edit-stock', component: EditStockComponent },
  {path : '', component : StockComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
