import { Component, OnInit } from '@angular/core';
import {StockService} from "../stock/stock.service";
import {Router} from "@angular/router";
import {Stock} from "../models/stock.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-edit-stock',
  templateUrl: './edit-stock.component.html'
})
export class EditStockComponent implements OnInit {

  stock: Stock;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private stockService: StockService) { }

  ngOnInit() {
    let stockId = localStorage.getItem("editStockId");
    if(!stockId) {
      alert("Invalid action.")
      this.router.navigate(['stocks']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      name: ['', Validators.required],
      currentPrice: ['', Validators.required],
      lastUpdate: []
    });
    this.stockService.getStockById(+stockId)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
  }

  onSubmit() {
    this.stockService.updateStock(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['stocks']);
        },
        error => {
          alert(error);
        });
  }

}
