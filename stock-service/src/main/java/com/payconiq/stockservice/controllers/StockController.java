package com.payconiq.stockservice.controllers;

import com.payconiq.stockservice.exception.StockNotFoundException;
import com.payconiq.stockservice.model.Stock;
import com.payconiq.stockservice.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class StockController {

    @Autowired
    private StockRepository stockRepository;

    @GetMapping("/stocks")
    public Iterable<Stock> getStocks() {
        Iterable<Stock> stocks = stockRepository.findAll();
        return stocks;
    }

    @GetMapping("/stock/{id}")
    public Stock getStock(@PathVariable Long id) {
        Stock stock = stockRepository.findById(id).orElseThrow(() -> new StockNotFoundException("Stock not found: " + id));
        return stock;
    }

    @PutMapping("/stocks/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Stock stockUpdated, @PathVariable long id) {
        if (stockUpdated.getCurrentPrice().compareTo(BigDecimal.ZERO) < 0) {
            throw new ConstraintViolationException("Should not be less than zero", null);
        }
        Stock stock = stockRepository.findById(id)
                .orElseThrow(() -> new StockNotFoundException("Stock not found: " + id));
        stock.setCurrentPrice(stockUpdated.getCurrentPrice());
        stock.setLastUpdate(new Date());
        stockRepository.save(stock);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/stocks")
    public ResponseEntity<Object> createStock(@RequestBody Stock stock) {
        if (stock.getCurrentPrice().compareTo(BigDecimal.ZERO) < 0) {
            throw new ConstraintViolationException("Should not be less than zero", null);
        }
        stock.setLastUpdate(new Date());
        stockRepository.save(stock);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}