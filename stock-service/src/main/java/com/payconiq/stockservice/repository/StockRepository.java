package com.payconiq.stockservice.repository;

import com.payconiq.stockservice.model.Stock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface StockRepository extends CrudRepository<Stock, Long> {
}
