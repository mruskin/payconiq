package com.payconiq.stockservice.exception;

import com.payconiq.stockservice.controllers.StockController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice(assignableTypes = {StockController.class})
public class RestExceptionHandler {

  private static final Logger LOG_WRONG_STOCK = LoggerFactory.getLogger(StockNotFoundException.class);
  private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

  @ExceptionHandler(StockNotFoundException.class)
  public ResponseEntity<Object> handleStockNotFoundException(StockNotFoundException ex) {
    LOG_WRONG_STOCK.error(ex.getLocalizedMessage());
    return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, ex.getMessage()));
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<Object> handleException(Exception ex) {
    LOG.error(ex.getLocalizedMessage(), ex);
    return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, "Something went wrong"));
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<Object> handleConstraintException(ConstraintViolationException ex) {
    LOG.error(ex.getLocalizedMessage(), ex);
    return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, "Validation failed"));
  }

  private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }

}
