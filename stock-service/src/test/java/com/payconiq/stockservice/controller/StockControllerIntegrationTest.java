package com.payconiq.stockservice.controller;

import com.payconiq.stockservice.controllers.StockController;
import com.payconiq.stockservice.exception.StockNotFoundException;
import com.payconiq.stockservice.model.Stock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StockControllerIntegrationTest {

    @Autowired
    StockController stockController;

    @Test
    public void testStockListIsCreated() {
        Iterable<Stock> stocks = stockController.getStocks();
        assertEquals(3, stocks.spliterator().estimateSize());
    }

    @Test
    public void testGetStockHappyPath() {
        Stock stock = stockController.getStock(1L);
        assertEquals("Intel", stock.getName());
    }

    @Test(expected = StockNotFoundException.class)
    public void testMissingStock() {
        stockController.getStock(20L);
    }

}
