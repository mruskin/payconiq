package com.payconiq.stockservice.controller;

import com.payconiq.stockservice.controllers.StockController;
import com.payconiq.stockservice.model.Stock;
import com.payconiq.stockservice.repository.StockRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StockController.class)
public class StockControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StockRepository stockRepository;

    @InjectMocks
    private StockController stockController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetStockList() throws Exception {
        Iterable<Stock> stocks = new ArrayList<>();
        Stock mockStock = new Stock();
        mockStock.setName("Twitter");
        ((ArrayList<Stock>) stocks).add(mockStock);

        Mockito.when(stockRepository.findAll())
                .thenReturn(stocks);

        mockMvc
                .perform(get("/api/stocks"))
                .andExpect(jsonPath("$[0].name", is("Twitter")))
                .andReturn();
    }

    @Test
    public void testGetStock() throws Exception {
        Stock mockStock = new Stock();
        mockStock.setName("Twitter");

        Mockito.when(stockRepository.findById(1L))
                .thenReturn(java.util.Optional.ofNullable(mockStock));

        mockMvc
                .perform(get("/api/stock/1"))
                .andExpect(jsonPath("$.name", is("Twitter")))
                .andReturn();
    }

    @Test
    public void testStockMissing() throws Exception {
        mockMvc
                .perform(get("/api/stock/10"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testStockCreated() throws Exception {
        MvcResult result = mockMvc
                .perform(post("/api/stocks").content("{\"name\" : \"Twitter\", \"currentPrice\" : \"12\"}").contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        MockHttpServletResponse response = result.getResponse();
        Assert.assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    }

    @Test
    public void testStockUpdate() throws Exception {
        Stock mockStock = new Stock();
        mockStock.setName("Twitter");

        Mockito.when(stockRepository.findById(1L))
                .thenReturn(java.util.Optional.ofNullable(mockStock));

        MvcResult result = mockMvc
                .perform(put("/api/stocks/1").content("{\"name\" : \"Twitter\", \"currentPrice\" : \"12\"}").contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        MockHttpServletResponse response = result.getResponse();
        Assert.assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatus());
    }

}
