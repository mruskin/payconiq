# payconiq assignment
The project consists of two parts.  
**stock-service** - spring boot REST API, back-end  
**stock-client** - Angular6, front-end

# Build back-end
```
mvn spring-boot:run
```
Runs on port 8080

# Build front-end
```
npm install
ng serve --proxy-config proxy.config.json
```
Runs on port 4200

# In memory data
List of stocks can be edited at: \stock-service\src\main\resources\data.sql
